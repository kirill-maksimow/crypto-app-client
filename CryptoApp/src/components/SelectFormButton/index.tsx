import React from "react";
import {
  Animated,
  StyleSheet,
  TouchableWithoutFeedback,
  Text,
} from "react-native";

interface ISelectFormButtonProps {
  title: string;
  backgroundColor: Animated.AnimatedInterpolation<string | number>;
  style?: any;
  onPress: () => void;
}

export const SelectFormButton = ({
  onPress,
  title,
  backgroundColor,
  style,
}: ISelectFormButtonProps) => {
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <Animated.View style={[styles.container, { backgroundColor }, style]}>
        <Text style={styles.title}>{title}</Text>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 45,
    width: "50%",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    color: "white",
    fontSize: 16,
  },
});
