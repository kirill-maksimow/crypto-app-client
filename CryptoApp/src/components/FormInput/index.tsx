import React from "react";
import {
  StyleSheet,
  Text,
  TextInput,
  TextInputProps,
  View,
} from "react-native";

interface IFormInputProps extends TextInputProps {
  title: string;
  error?: boolean | string;
}

export const FormInput = (props: IFormInputProps) => {
  const { title, error, placeholder } = props;
  return (
    <>
      <View style={styles.textContainer}>
        <Text style={styles.title}>{title}</Text>
        {error && <Text style={styles.errorText}>{error}</Text>}
      </View>
      <TextInput {...props} placeholder={placeholder} style={styles.input} />
    </>
  );
};

const styles = StyleSheet.create({
  title: {
    fontWeight: "bold",
  },
  input: {
    borderWidth: 1,
    borderColor: "#1b1b33",
    height: 35,
    borderRadius: 8,
    fontSize: 16,
    paddingLeft: 10,
    marginBottom: 20,
  },
  textContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5,
  },
  errorText: {
    color: "red",
    fontSize: 16,
  },
});
