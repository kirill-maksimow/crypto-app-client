import React, { useEffect, useContext } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { LoginScreen } from "../screens/LoginScreen";
import { MainScreen } from "../screens/MainScreen";
import { Spinner } from "../components/Spinner";
import { observer } from "mobx-react-lite";
import { AuthContext, IAuthContext } from "../context/AuthContext";

const AuthStack = createNativeStackNavigator();

const MainStack = createNativeStackNavigator();

const MainStackNavigator = () => {
  return (
    <MainStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <MainStack.Screen name="MainScreen" component={MainScreen} />
      <MainStack.Screen name="LoginScreen" component={LoginScreen} />
    </MainStack.Navigator>
  );
};

const AuthStackNavigator = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <AuthStack.Screen name="LoginScreen" component={LoginScreen} />
      <AuthStack.Screen name="MainScreen" component={MainStackNavigator} />
    </AuthStack.Navigator>
  );
};

export const Navigator = observer(() => {
  const { isLoading, isAuth, checkingAuth } = useContext(
    AuthContext
  ) as IAuthContext;

  useEffect(() => {
    checkingAuth();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <NavigationContainer>
      {!isAuth ? <AuthStackNavigator /> : <MainStackNavigator />}
    </NavigationContainer>
  );
});
