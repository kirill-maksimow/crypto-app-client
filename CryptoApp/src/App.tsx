/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */
import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { AuthProvider } from "./context/AuthContext";
import { Navigator } from "./navigator/Navigator";

const App = () => {
  return (
    <SafeAreaView style={styles.container}>
      <AuthProvider>
        <Navigator />
      </AuthProvider>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
