import React, { createContext, ReactNode, useState } from "react";
import * as Keychain from "react-native-keychain";
import Store from "../store/store";

import { login, registration, logout } from "../services/AuthService";
import { IUser } from "../models/IUser";
import { AuthResponse } from "../models/response/AuthResponse";
import axios from "axios";
import { API_URL } from "../http";

interface IProviderProps {
  children: ReactNode;
}

export interface IAuthContext {
  isLoading: boolean;
  isAuth: boolean;
  loginUser: (email: string, password: string) => void;
  registerUser: (name: string, password: string, email: string) => void;
  logoutUser: () => void;
  checkingAuth: () => void;
}

export const AuthContext = createContext<IAuthContext | null>(null);

export const AuthProvider = ({ children }: IProviderProps) => {
  const { setUser } = new Store();

  const [isLoading, setIsLoading] = useState(false);
  const [isAuth, setIsAuth] = useState(false);

  const registerUser = async (
    name: string,
    email: string,
    password: string
  ) => {
    try {
      setIsLoading(true);
      const resp = await registration(name, email, password);
      await Keychain.setGenericPassword(
        "token",
        JSON.stringify({
          accessToken: resp.data.accesToken,
          refreshToken: resp.data.refreshToken,
        })
      );
      setIsAuth(true);
      setIsLoading(false);
      setUser(resp.data.user);
    } catch (error) {
      console.log(error);
    }
  };

  const loginUser = async (email: string, password: string) => {
    try {
      setIsLoading(true);
      const resp = await login(email, password);
      await Keychain.setGenericPassword(
        "token",
        JSON.stringify({
          accessToken: resp.data.accesToken,
          refreshToken: resp.data.refreshToken,
        })
      );
      setIsAuth(true);
      setIsLoading(false);
      setUser(resp.data.user);
    } catch (e) {
      console.log(e);
    }
  };

  const checkingAuth = async () => {
    try {
      setIsLoading(true);
      const value = await Keychain.getGenericPassword();
      if (value) {
        const jwt = JSON.parse(value.password);
        const resp = await axios.post<AuthResponse>(
          `${API_URL}/users/refresh`,
          { refreshToken: jwt.refreshToken },
          {
            headers: {
              authorization: `Bearer ${jwt.accessToken}`,
            },
          }
        );
        await Keychain.setGenericPassword(
          "token",
          JSON.stringify({
            accessToken: resp.data.accesToken,
            refreshToken: resp.data.refreshToken,
          })
        );
        setIsAuth(true);
        setIsLoading(false);
      }
      setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  const logoutUser = async () => {
    try {
      setIsLoading(true);
      const value = await Keychain.getGenericPassword();

      if (value) {
        const jwt = JSON.parse(value.password);
        await logout(jwt.refreshToken);
        await Keychain.resetGenericPassword();
        setIsAuth(false);
        setUser({} as IUser);
      }
      setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
    setIsAuth(false);
  };

  return (
    <AuthContext.Provider
      value={{
        isLoading,
        isAuth,
        loginUser,
        registerUser,
        logoutUser,
        checkingAuth,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
