import axios from "axios";
import * as Keychain from "react-native-keychain";

export const API_URL = "http://localhost:9090";

const $api = axios.create({
  baseURL: API_URL,
});

$api.interceptors.request.use(
  async (config) => {
    if (!config.headers?.Authorization) {
      const value = await Keychain.getGenericPassword();
      if (value) {
        const jwt = JSON.parse(value.password);
        if (config.headers) {
          config.headers.Authorization = `Bearer ${jwt.accessToken}`;
        }
      }
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default $api;
