import { IUser } from "../models/IUser";
import { makeAutoObservable } from "mobx";

export default class Store {
  user = {} as IUser;

  constructor() {
    makeAutoObservable(this);
  }

  setUser(user: IUser) {
    this.user = user;
  }
}
