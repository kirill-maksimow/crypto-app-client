import { AxiosResponse } from "axios";
import $api from "../http";
import { AuthResponse } from "../models/response/AuthResponse";

async function login(
  email: string,
  password: string
): Promise<AxiosResponse<AuthResponse>> {
  return $api.post<AuthResponse>("/users/login", {
    email,
    password,
  });
}

async function registration(
  name: string,
  email: string,
  password: string
): Promise<AxiosResponse<AuthResponse>> {
  return $api.post<AuthResponse>("/users/registration", {
    name,
    email,
    password,
  });
}

async function logout(refreshToken: string): Promise<void> {
  $api.post<AuthResponse>("/users/logout/", { refreshToken });
}

export { login, registration, logout };
