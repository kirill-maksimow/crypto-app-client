import React, { useRef } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Animated,
  useWindowDimensions,
} from "react-native";
import { SelectFormButton } from "../../components/SelectFormButton";
import { LoginForm } from "./components/LoginForm";
import { LoginScreenHeader } from "./components/LoginScreenHeader";
import { SignUpForm } from "./components/SignUpForm";

export const LoginScreen = () => {
  const { width } = useWindowDimensions();

  const scrollViewRef = useRef<any>();

  const animation = useRef(new Animated.Value(0)).current;
  const textOpacity = animation.interpolate({
    inputRange: [0, width],
    outputRange: [1, 0],
  });

  const leftTextTranslateX = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, 40],
  });

  const righTextTranslateY = animation.interpolate({
    inputRange: [0, width],
    outputRange: [0, -20],
  });

  const loginButtonColor = animation.interpolate({
    inputRange: [0, width],
    outputRange: ["rgba(27,27,51,1)", "rgba(27,27,51,0.4)"],
  });

  const signUpButtonColor = animation.interpolate({
    inputRange: [0, width],
    outputRange: ["rgba(27,27,51,0.4)", "rgba(27,27,51,1)"],
  });

  const onPressLoginButton = () => {
    scrollViewRef.current.scrollTo({ x: 0 });
  };
  const onPressSignUpButton = () => {
    scrollViewRef.current.scrollToEnd();
  };
  return (
    <View style={styles.container}>
      <LoginScreenHeader
        righTextOpacity={textOpacity}
        leftTextTranslateX={leftTextTranslateX}
        righTextTranslateY={righTextTranslateY}
      />
      <View style={styles.buttonContainer}>
        <SelectFormButton
          onPress={onPressLoginButton}
          backgroundColor={loginButtonColor}
          title="Login"
          style={styles.leftButton}
        />
        <SelectFormButton
          onPress={onPressSignUpButton}
          backgroundColor={signUpButtonColor}
          title="Sign Up"
          style={styles.rigthButton}
        />
      </View>
      <ScrollView
        ref={scrollViewRef}
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        onScroll={Animated.event(
          [{ nativeEvent: { contentOffset: { x: animation } } }],
          { useNativeDriver: false }
        )}
        pagingEnabled
        horizontal
        style={styles.scrollView}
      >
        <LoginForm />
        <ScrollView>
          <SignUpForm />
        </ScrollView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonContainer: {
    flexDirection: "row",
    marginTop: 20,
    paddingHorizontal: 20,
  },
  leftButton: {
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
  },
  rigthButton: {
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
  },
  scrollView: {
    marginTop: 20,
  },
});
