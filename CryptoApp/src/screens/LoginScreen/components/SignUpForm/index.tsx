import React, { useContext } from "react";
import { FormInput } from "../../../../components/FormInput";
import { FormContainer } from "../FormContainer";
import { FormSubmitButton } from "../FormSubmitButton";
import { Formik, FormikHelpers } from "formik";
import * as Yup from "yup";
import { AuthContext, IAuthContext } from "../../../../context/AuthContext";

const validationSchema = Yup.object({
  name: Yup.string()
    .trim()
    .min(3, "Invalid name!")
    .required("Name is required!"),
  email: Yup.string().email("Invalid email!").required("Email is required!"),
  password: Yup.string()
    .trim()
    .min(8, "Password is too short!")
    .required("Password is required!"),
  confirmedPassword: Yup.string().test(
    "password-match",
    "Passwords do not match",
    function () {
      const { password } = this.parent;
      return password === this.parent.confirmedPassword;
    }
  ),
});

interface SignupValues {
  name: string;
  email: string;
  password: string;
  confirmedPassword: string;
}

export const SignUpForm = () => {
  const { registerUser } = useContext(AuthContext) as IAuthContext;

  const userInfo = {
    name: "",
    email: "",
    password: "",
    confirmedPassword: "",
  };

  const submitHandler = async (
    values: SignupValues,
    formikActions: FormikHelpers<SignupValues>
  ) => {
    await registerUser(values.name, values.email, values.password);
    formikActions.resetForm();
    formikActions.setSubmitting(false);
  };

  return (
    <FormContainer>
      <Formik
        initialValues={userInfo}
        validationSchema={validationSchema}
        onSubmit={submitHandler}
      >
        {({
          handleChange,
          handleSubmit,
          touched,
          values,
          isSubmitting,
          errors,
          handleBlur,
        }) => (
          <>
            <FormInput
              value={values.name}
              error={touched.name && errors.name}
              title="Name"
              onBlur={handleBlur("name")}
              placeholder="Jhon Jhonson"
              onChangeText={handleChange("name")}
            />
            <FormInput
              value={values.email}
              autoCapitalize="none"
              error={touched.email && errors.email}
              title="Email"
              placeholder="example@email.com"
              onBlur={handleBlur("email")}
              onChangeText={handleChange("email")}
            />
            <FormInput
              value={values.password}
              autoCapitalize="none"
              error={touched.password && errors.password}
              title="Password"
              placeholder="******"
              secureTextEntry
              onBlur={handleBlur("password")}
              onChangeText={handleChange("password")}
            />
            <FormInput
              value={values.confirmedPassword}
              autoCapitalize="none"
              error={touched.confirmedPassword && errors.confirmedPassword}
              title="Confirm Password"
              placeholder="******"
              secureTextEntry
              onBlur={handleBlur("confirmedPassword")}
              onChangeText={handleChange("confirmedPassword")}
            />
            <FormSubmitButton
              onPress={handleSubmit}
              submitting={isSubmitting}
              title="Sign up"
            />
          </>
        )}
      </Formik>
    </FormContainer>
  );
};
