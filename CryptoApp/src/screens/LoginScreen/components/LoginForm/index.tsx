import { Formik, FormikHelpers } from "formik";
import React, { useContext } from "react";
import { View } from "react-native";
import { FormInput } from "../../../../components/FormInput";
import { FormContainer } from "../FormContainer";
import { FormSubmitButton } from "../FormSubmitButton";
import * as Yup from "yup";
import { observer } from "mobx-react-lite";
import { AuthContext, IAuthContext } from "../../../../context/AuthContext";

interface Values {
  email: string;
  password: string;
}

const validationSchema = Yup.object({
  email: Yup.string().email("Invalid email!").required("Email is required!"),
  password: Yup.string().trim().required("Password is required!"),
});

export const LoginForm = observer(() => {
  const { loginUser } = useContext(AuthContext) as IAuthContext;
  const inputValues = {
    email: "",
    password: "",
  };

  const submitHandler = async (
    values: Values,
    formikActions: FormikHelpers<Values>
  ) => {
    try {
      await loginUser(values.email, values.password);
      formikActions.resetForm();
      formikActions.setSubmitting(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <FormContainer>
      <Formik
        initialValues={inputValues}
        validationSchema={validationSchema}
        onSubmit={submitHandler}
      >
        {({
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          touched,
          errors,
          values,
        }) => (
          <View>
            <FormInput
              value={values.email}
              error={touched.email && errors.email}
              onChangeText={handleChange("email")}
              onBlur={handleBlur("email")}
              title="Email"
              placeholder="example@email.com"
              autoCapitalize="none"
            />
            <FormInput
              value={values.password}
              title="Password"
              error={touched.password && errors.password}
              placeholder="******"
              onBlur={handleBlur("password")}
              onChangeText={handleChange("password")}
              autoCapitalize="none"
              secureTextEntry
            />
            <FormSubmitButton
              submitting={isSubmitting}
              onPress={handleSubmit}
              title="Login"
            />
          </View>
        )}
      </Formik>
    </FormContainer>
  );
});
