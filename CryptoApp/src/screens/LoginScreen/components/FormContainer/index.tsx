import React from "react";
import { Dimensions, StyleSheet } from "react-native";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

interface IFormContainerProps {
  children: React.ReactNode;
}

export const FormContainer = ({ children }: IFormContainerProps) => {
  return (
    <KeyboardAwareScrollView
      style={styles.keyboardAware}
      contentContainerStyle={[
        styles.keyboardAwareContent,
        { paddingBottom: 200 },
      ]}
      keyboardShouldPersistTaps="handled"
      enableOnAndroid
    >
      {children}
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get("window").width,
    paddingHorizontal: 20,
  },
  keyboardAware: {
    width: Dimensions.get("window").width,
    paddingHorizontal: 20,
  },
  keyboardAwareContent: {},
});
