import React from "react";
import { View, Text, StyleSheet, Animated } from "react-native";

interface ILoginScreenHeader {
  leftTextTranslateX: Animated.AnimatedInterpolation<number | string>;
  righTextTranslateY: Animated.AnimatedInterpolation<number | string>;
  righTextOpacity: Animated.AnimatedInterpolation<number | string>;
}

export const LoginScreenHeader = ({
  leftTextTranslateX,
  righTextTranslateY,
  righTextOpacity,
}: ILoginScreenHeader) => {
  return (
    <View style={styles.container}>
      <View style={styles.topTextContainer}>
        <Animated.Text
          style={[
            styles.topTitleText,
            { transform: [{ translateX: leftTextTranslateX }] },
          ]}
        >
          Welcome
        </Animated.Text>
        <Animated.Text
          style={[
            styles.topTitleText,
            {
              opacity: righTextOpacity,
              transform: [{ translateY: righTextTranslateY }],
            },
          ]}
        >
          Back
        </Animated.Text>
      </View>
      <Text style={styles.titleText}>Crypto Manager App</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 80,
    justifyContent: "center",
    alignItems: "center",
  },
  topTextContainer: {
    flexDirection: "row",
  },
  topTitleText: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#1b1b33",
    padding: 4,
  },
  titleText: {
    fontSize: 16,
    color: "#1b1b33",
  },
});
