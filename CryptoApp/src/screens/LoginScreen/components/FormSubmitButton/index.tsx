import React from "react";
import {
  TouchableOpacity,
  Text,
  StyleSheet,
  ActivityIndicator,
} from "react-native";

interface IFormSubmitButton {
  title: string;
  submitting: boolean;
  onPress: () => void;
}
export const FormSubmitButton = ({
  title,
  submitting,
  onPress,
}: IFormSubmitButton) => {
  return (
    <TouchableOpacity
      style={[
        styles.container,
        {
          backgroundColor: submitting
            ? "rgba(27,27,51,1)"
            : "rgba(27,27,51,0.4)",
        },
      ]}
      onPress={onPress}
      disabled={submitting}
    >
      {submitting ? (
        <ActivityIndicator />
      ) : (
        <Text style={styles.title}>{title}</Text>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 45,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 18,
    color: "white",
  },
});
