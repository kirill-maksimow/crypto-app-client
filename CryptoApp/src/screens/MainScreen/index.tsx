import { observer } from "mobx-react-lite";
import React, { useContext } from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { AuthContext, IAuthContext } from "../../context/AuthContext";

export const MainScreen = observer(() => {
  const { logoutUser } = useContext(AuthContext) as IAuthContext;

  const handleLogout = async () => {
    logoutUser();
  };
  return (
    <View>
      <Text>Main Screen</Text>
      <TouchableOpacity onPress={handleLogout}>
        <Text>Logout</Text>
      </TouchableOpacity>
    </View>
  );
});
